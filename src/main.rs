use multipart::client::lazy::Multipart;
use percent_encoding::{utf8_percent_encode, NON_ALPHANUMERIC};
use serde::Deserialize;
use std::{
    collections::HashMap,
    io::{Read, Write},
};

#[derive(Deserialize, Debug)]
struct User {
    dst_token: String,
    dst_id: i64,
}

#[derive(Deserialize, Debug)]
struct Config {
    src_root_group_id: String,
    src_gitlab_token: String,
    dst_gitlab_token: String,
    src_gitlab_api: String,
    dst_gitlab_api: String,
    default_dst_user_id: i64,
    default_dst_user_token: String,
    projects_whitelist: Option<Vec<String>>,
    src_users: HashMap<String, User>,
}

#[derive(Debug)]
struct Migrator {
    config: Config,
}

enum Gitlab {
    Source,
    Destination,
}

impl Migrator {
    fn request(
        &self,
        request_function: for<'r> fn(&'r str) -> ureq::Request,
        gitlab: Gitlab,
        url: &str,
    ) -> ureq::Request {
        match gitlab {
            Gitlab::Source => {
                request_function(format!("{}/{}", self.config.src_gitlab_api, url).as_str())
                    .set("PRIVATE-TOKEN", &self.config.src_gitlab_token)
            }
            Gitlab::Destination => {
                request_function(format!("{}/{}", self.config.dst_gitlab_api, url).as_str())
                    .set("PRIVATE-TOKEN", &self.config.dst_gitlab_token)
            }
        }
    }

    fn request_call(
        &self,
        request_function: for<'r> fn(&'r str) -> ureq::Request,
        gitlab: Gitlab,
        url: &str,
    ) -> ureq::Response {
        let request = self.request(request_function, gitlab, url);
        loop {
            let res = request.clone().call();
            if let Ok(res) = res {
                return res;
            } else {
                println!("request error, retrying ({})", res.err().unwrap());
            }
        }
    }

    fn get_dst_user_token(&self, username: &str) -> &str {
        if let Some(user) = self.config.src_users.get(username) {
            &user.dst_token
        } else {
            &self.config.default_dst_user_token
        }
    }

    fn get_dst_user_id(&self, username: &str) -> i64 {
        if let Some(user) = self.config.src_users.get(username) {
            user.dst_id
        } else {
            self.config.default_dst_user_id
        }
    }

    fn get_projects(&self) -> Result<Vec<serde_json::Value>, Box<dyn std::error::Error>> {
        print!("    Fetching projects from source Gitlab... ");
        std::io::stdout().flush().unwrap();
        let mut projects: Vec<serde_json::Value> = vec![];
        self.get_projects_for_group(&mut projects, &self.config.src_root_group_id)?;
        println!("done");
        Ok(projects)
    }

    fn get_projects_for_group<T: std::fmt::Display + ?Sized>(
        &self,
        projects: &mut Vec<serde_json::Value>,
        group_id: &T,
    ) -> Result<(), Box<dyn std::error::Error>> {
        self.populate_projects_from_group(projects, group_id)?;

        let subgroups = self
            .request_call(
                ureq::get,
                Gitlab::Source,
                format!("groups/{}/subgroups", group_id).as_str(),
            )
            .into_json::<serde_json::Value>()?;

        for obj in subgroups.as_array().unwrap() {
            self.get_projects_for_group(projects, &obj["id"].as_u64().unwrap())?;
        }

        Ok(())
    }

    fn populate_projects_from_group<T: std::fmt::Display + ?Sized>(
        &self,
        projects: &mut Vec<serde_json::Value>,
        group_id: &T,
    ) -> Result<(), Box<dyn std::error::Error>> {
        let group = self
            .request_call(
                ureq::get,
                Gitlab::Source,
                format!("groups/{}", group_id).as_str(),
            )
            .into_json::<serde_json::Value>()?;

        for obj in group["projects"].as_array().unwrap() {
            projects.push(obj.clone());
        }

        Ok(())
    }
}

struct Project<'a> {
    migrator: &'a Migrator,
    project_path: String,
    project_path_encoded: String,
    src_project_details: serde_json::Value,
    issues: Vec<serde_json::Value>,
    merge_requests: Vec<serde_json::Value>,
}

impl<'a> Project<'a> {
    fn new(migrator: &'a Migrator, src_project_path: &str) -> Self {
        // Check that source exists
        let res = ureq::get(&format!(
            "{}/projects/{}",
            migrator.config.src_gitlab_api,
            utf8_percent_encode(src_project_path, NON_ALPHANUMERIC)
        ))
        .set("PRIVATE-TOKEN", &migrator.config.src_gitlab_token)
        .call()
        .unwrap();

        Project {
            migrator,
            project_path: src_project_path.to_string(),
            project_path_encoded: utf8_percent_encode(src_project_path, NON_ALPHANUMERIC)
                .to_string(),
            src_project_details: res.into_json().unwrap(),
            issues: Vec::new(),
            merge_requests: Vec::new(),
        }
    }

    // Repository export/import handling

    fn start_export(&self) -> Result<(), Box<dyn std::error::Error>> {
        println!("{}: Starting export", self.project_path);
        self.migrator
            .request_call(
                ureq::post,
                Gitlab::Source,
                format!("projects/{}/export", self.project_path_encoded).as_str(),
            )
            .into_json::<serde_json::Value>()?;
        Ok(())
    }

    fn wait_for_export_complete(&self) -> Result<(), Box<dyn std::error::Error>> {
        println!("{}: Exporting", self.project_path);
        let mut status: String = "init".to_string();
        while status != "finished" {
            std::thread::sleep(std::time::Duration::from_secs(5));

            let response = self
                .migrator
                .request_call(
                    ureq::get,
                    Gitlab::Source,
                    format!("projects/{}/export", self.project_path_encoded).as_str(),
                )
                .into_json::<serde_json::Value>()?;
            status = response["export_status"].as_str().unwrap().to_string();
        }
        println!("{}: Export complete", self.project_path);
        Ok(())
    }

    fn download_export(&self) -> Result<impl Send + std::io::Read, Box<dyn std::error::Error>> {
        println!("{}: Downloading export", self.project_path);
        let export_req = self.migrator.request_call(
            ureq::get,
            Gitlab::Source,
            format!("projects/{}/export/download", self.project_path_encoded).as_str(),
        );
        let export = export_req.into_reader();
        Ok(export)
    }

    fn delete_if_exists(&self) -> Result<(), Box<dyn std::error::Error>> {
        if self
            .migrator
            .request(
                ureq::get,
                Gitlab::Destination,
                &format!("projects/{}", self.project_path_encoded),
            )
            .call()
            .is_ok()
        {
            println!("{}: Deleting existing project", self.project_path);
            self.migrator.request_call(
                ureq::delete,
                Gitlab::Destination,
                &format!("projects/{}", self.project_path_encoded),
            );
        }
        while self
            .migrator
            .request(
                ureq::get,
                Gitlab::Destination,
                &format!("projects/{}", self.project_path_encoded),
            )
            .call()
            .is_ok()
        {
            std::thread::sleep(std::time::Duration::from_secs(5));
            println!("{}: Still waiting for deletion", self.project_path);
        }
        Ok(())
    }

    fn import_project(&self, export: impl Read) -> Result<(), Box<dyn std::error::Error>> {
        println!("{}: Preparing form", self.project_path);
        let mut mpart = Multipart::new();
        mpart.add_text(
            "namespace",
            self.src_project_details["namespace"]["full_path"]
                .as_str()
                .unwrap(),
        );
        mpart.add_text("name", self.src_project_details["name"].as_str().unwrap());
        mpart.add_text("path", self.src_project_details["path"].as_str().unwrap());
        mpart.add_text("overwrite", "true");
        mpart.add_stream("file", export, Some("file.tar.gz"), None);

        // Duplicate the multipart form to be able to send it a second time in case of failure
        let mut mdata = mpart.prepare().unwrap();
        let mut mdata_content: Vec<u8> = Vec::new();
        mdata.read_to_end(&mut mdata_content)?;
        let mdata_boundary = mdata.boundary();

        println!("{}: Importing", self.project_path);

        while let Err(e) = self
            .migrator
            .request(ureq::post, Gitlab::Destination, "projects/import")
            .set(
                "Content-Type",
                &format!("multipart/form-data; boundary={}", mdata_boundary),
            )
            .send(&*mdata_content.clone())
        {
            println!(
                "{}: Import failed, retrying in 15 seconds",
                self.project_path
            );
            println!("{}: error: {}", self.project_path, e);
            std::thread::sleep(std::time::Duration::from_secs(15));
        }

        println!("{}: Import complete", self.project_path);

        Ok(())
    }

    fn migrate_project(&self) -> Result<(), Box<dyn std::error::Error>> {
        println!("{}: Beginning", self.project_path);
        self.start_export()?;

        self.wait_for_export_complete()?;

        let export = self.download_export()?;

        self.delete_if_exists()?;
        self.import_project(export)?;

        println!("{}: Finished", self.project_path);
        Ok(())
    }

    // Issue handling

    fn build_issue_list(&mut self) -> Result<(), Box<dyn std::error::Error>> {
        let mut page = Some("1".to_string());
        while page.is_some() {
            let issue_page = self.migrator.request_call(
                ureq::get,
                Gitlab::Source,
                &format!(
                    "projects/{}/issues?per_page=100&page={}",
                    self.project_path_encoded,
                    page.as_ref().unwrap()
                ),
            );
            if issue_page.header("x-page").unwrap() != issue_page.header("x-total-pages").unwrap() {
                page = Some(issue_page.header("x-next-page").unwrap().to_owned());
            } else {
                page = None
            }
            self.issues
                .append(&mut issue_page.into_json::<Vec<serde_json::Value>>()?);
        }
        Ok(())
    }

    fn migrate_issues_links(&self) -> Result<(), Box<dyn std::error::Error>> {
        println!("{} Migrating issues links", self.project_path);
        println!("    issue count: {:#?}", self.issues.len());
        for issue in self.issues.iter() {
            let links = self
                .migrator
                .request_call(
                    ureq::get,
                    Gitlab::Source,
                    &format!(
                        "projects/{}/issues/{}/links",
                        self.project_path_encoded, issue["iid"],
                    ),
                )
                .into_json::<Vec<serde_json::Value>>()?;
            if !links.is_empty() {
                println!(
                    "\n    project {} issue {} ({}): links count: {}",
                    issue["project_id"],
                    issue["title"],
                    issue["iid"],
                    links.len()
                );
                for link in &links {
                    let src_project = self
                        .migrator
                        .request_call(
                            ureq::get,
                            Gitlab::Source,
                            format!("projects/{}", link["project_id"]).as_str(),
                        )
                        .into_json::<serde_json::Value>()?;
                    print!(
                        "    linked issue: ({} - {}) {} - ",
                        src_project["path_with_namespace"], link["iid"], link["title"]
                    );
                    match self
                        .migrator
                        .request(
                            ureq::post,
                            Gitlab::Destination,
                            format!(
                                "projects/{}/issues/{}/links",
                                self.project_path_encoded, issue["iid"]
                            )
                            .as_str(),
                        )
                        .query(
                            "target_project_id",
                            src_project["path_with_namespace"].as_str().unwrap(),
                        )
                        .query("target_issue_iid", &link["iid"].to_string())
                        .call()
                    {
                        Ok(_) => println!("created"),
                        Err(ureq::Error::Status(409, _)) => {
                            println!("link already exists")
                        }
                        Err(e) => {
                            println!("link creation error: {}", e)
                        }
                    }
                }
            }
        }
        Ok(())
    }

    fn migrate_issues_assignee(&self) -> Result<(), Box<dyn std::error::Error>> {
        println!("{} Migrating issues assignee", self.project_path);
        println!("    issue count: {:#?}", self.issues.len());
        for issue in self.issues.iter() {
            if let Some(assignee) = &issue["assignees"][0]["username"].as_str() {
                let assignee_id = self.migrator.get_dst_user_id(assignee);
                match self
                    .migrator
                    .request(
                        ureq::put,
                        Gitlab::Destination,
                        format!(
                            "projects/{}/issues/{}",
                            self.project_path_encoded, issue["iid"]
                        )
                        .as_str(),
                    )
                    .query("assignee_ids", &format!("{}", assignee_id))
                    .call()
                {
                    Ok(_) => println!(
                        "{}: issue #{} assigned to {} ({})",
                        self.project_path, issue["iid"], assignee, assignee_id
                    ),
                    Err(e) => {
                        println!(
                            "{}: issue #{} assignation error: {}",
                            self.project_path, issue["iid"], e
                        )
                    }
                }
            } else {
                println!(
                    "{}: issue #{} is unassigned",
                    self.project_path, issue["iid"]
                );
            }
        }
        Ok(())
    }

    // MR approvals handling

    fn build_mr_list(&mut self) -> Result<(), Box<dyn std::error::Error>> {
        let mut page = Some("1".to_string());
        while page.is_some() {
            let mr_page = self.migrator.request_call(
                ureq::get,
                Gitlab::Source,
                &format!(
                    "projects/{}/merge_requests?per_page=100&page={}",
                    self.project_path_encoded,
                    page.as_ref().unwrap()
                ),
            );
            if mr_page.header("x-page").unwrap() != mr_page.header("x-total-pages").unwrap() {
                page = Some(mr_page.header("x-next-page").unwrap().to_owned());
            } else {
                page = None
            }
            self.merge_requests
                .append(&mut mr_page.into_json::<Vec<serde_json::Value>>()?);
        }
        Ok(())
    }

    fn migrate_mr_approvals(&self) -> Result<(), Box<dyn std::error::Error>> {
        println!("{}: Migrating MR approvals", self.project_path);
        println!("    MR count: {:#?}", self.merge_requests.len());
        for mr in self.merge_requests.iter() {
            let approval_state = self
                .migrator
                .request_call(
                    ureq::get,
                    Gitlab::Source,
                    &format!(
                        "projects/{}/merge_requests/{}/approvals",
                        self.project_path_encoded, mr["iid"],
                    ),
                )
                .into_json::<serde_json::Value>()?;
            let approvals = match approval_state["approved_by"].as_array() {
                Some(approvals) => approvals,
                None => continue,
            };
            if !approvals.is_empty() {
                println!(
                    "\n    project {} MR {} ({}): approvals count: {}",
                    mr["project_id"],
                    mr["title"],
                    mr["iid"],
                    approvals.len()
                );
                for approval in approvals {
                    let src_approver_username = approval["user"]["username"].as_str().unwrap();
                    // Get the approver of the MR in the destination to log its mapping
                    let dst_approver = self
                        .migrator
                        .request_call(
                            ureq::get,
                            Gitlab::Destination,
                            format!(
                                "users/{}",
                                self.migrator.get_dst_user_id(src_approver_username)
                            )
                            .as_str(),
                        )
                        .into_json::<serde_json::Value>()?;
                    println!(
                        "    MR approved by {}, now {}",
                        src_approver_username, dst_approver["username"]
                    );

                    // Approve the MR with the user's destination token
                    match ureq::post(
                        format!(
                            "{}/projects/{}/merge_requests/{}/approve",
                            self.migrator.config.dst_gitlab_api,
                            self.project_path_encoded,
                            mr["iid"]
                        )
                        .as_str(),
                    )
                    .set(
                        "PRIVATE-TOKEN",
                        self.migrator
                            .get_dst_user_token(approval["user"]["username"].as_str().unwrap()),
                    )
                    .call()
                    {
                        Ok(_) => println!("created"),
                        Err(e) => {
                            println!(
                                "approval error for MR !{} and user {}: {}",
                                mr["iid"], dst_approver["username"], e
                            )
                        }
                    }
                }
            }
        }
        Ok(())
    }
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let migrator = Migrator {
        config: toml::from_str(&std::fs::read_to_string(format!(
            "{}/.config/gitlab_to_gitlab.toml",
            std::env::var("HOME").map_err(|err| format!("{}: HOME", err))?
        ))?)
        .map_err(|err| format!("configuration: {}", err))?,
    };
    println!("    Loaded configuration:\n{:#?}", &migrator.config);

    let mut projects = migrator.get_projects()?;

    if let Some(ref whitelist) = migrator.config.projects_whitelist {
        projects = projects
            .into_iter()
            .filter(|x| {
                whitelist
                    .iter()
                    .any(|y| x["path_with_namespace"].as_str().unwrap() == y)
            })
            .collect::<Vec<serde_json::Value>>();
    }

    println!(
        "    These projects have been listed ({}): {:#?}",
        projects.len(),
        projects
            .iter()
            .map(|x| { x["path_with_namespace"].as_str().unwrap() })
            .collect::<Vec<&str>>()
    );

    let mut projects: Vec<Project> = projects
        .into_iter()
        .map(|project| {
            let project_path = project["path_with_namespace"].as_str().unwrap();
            Project::new(&migrator, project_path)
        })
        .collect();

    // Start by deleting everything because Gitlab does that asynchronously
    for project in &mut projects {
        project.delete_if_exists()?;
    }

    // Then migrate everything
    for project in &mut projects {
        project.migrate_project()?;
    }

    // Check that everything is finished importing before going to the next stage
    let mut finished = false;
    while !finished {
        finished = true;
        println!("\n    Still checking if everything is finished...");
        for project in &projects {
            let res = migrator
                .request(
                    ureq::get,
                    Gitlab::Destination,
                    &format!("projects/{}/import", project.project_path_encoded),
                )
                .call();
            if let Ok(res) = res {
                let status = res.into_json::<serde_json::Value>().unwrap();
                if status["import_status"] == "failed" {
                    println!("{}: Import failed, retrying", project.project_path);
                    project.migrate_project()?;
                }
                if status["import_status"] != "finished" {
                    finished = false;
                    println!(
                        "{}: Import status: {}",
                        project.project_path, status["import_status"]
                    );
                }
            }
        }
        std::thread::sleep(std::time::Duration::from_secs(5));
    }

    // Then migrate stuff that needs all projects to be fully imported in the destination
    for project in &mut projects {
        project.build_mr_list()?;
        project.migrate_mr_approvals()?;
        project.build_issue_list()?;
        project.migrate_issues_links()?;
        project.migrate_issues_assignee()?;
    }

    Ok(())
}
