# Gitlab to gitlab

This tool recursively migrates all the projects tree of a group to the same tree on another Gitlab instance.
The group tree must have been already migrated on the destination, using Gitlab's group export/import feature.

The only needed configuration is as follow, and must be placed in `$HOME/.config/gitlab_to_gitlab.toml`:

```toml
root_group_id = "1234"
src_gitlab_token = "api_token_with_read_api_privileges"
dst_gitlab_token = "api_token_with_api_and_write_repository_privileges"
src_gitlab_api = "https://gitlab.sour.ce/api/v4"
dst_gitlab_api = "https://gitlab.destinati.on/api/v4"

# Default user for MR approval in case approver doesn't exist in destination Gitlab
default_dst_user_id = 0
default_dst_user_token = "default-token"

# This setting is optional, and allows to filter the migrated projects
projects_whitelist = [
  "my_group/my_first_project",
  "my_group/my_second_project",
  # "my_group/my_third_project_not_migrated",
]

# Map source usernames to destination IDs and tokens for MR approvals
[src_users]
[src_users.SrcUsername]
dst_id = 7
dst_token = "user1-token"
[src_users.AnotherSrcUserName]
dst_id = 9
dst_token = "user2-token"
```

It's very rough, but it was used to migrate a +80 projects / ~10 subgroups in about 1h with success. Feel free to send
Merge Requests if you made improvements.
